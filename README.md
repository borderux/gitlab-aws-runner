# Gitlab AWS Scaling Runner Config

All Border projects require CI/CD runners so that members/devs are not required to have accounts to use the Gitlab shared runners.  If a developer does not have their account setup with billing information, they will get this error when their change kicks of a pipeline build. \
![Pipeline build error](runner-error.png) \
Setting up your own AWS runners and connecting to Gitlab proved to be a long and difficult task.  This project contains the documentation and notes to configure a scaling runner on AWS that uses spot instances for minimal cost.  This is based on the documentation found in [Autoscaling Gitlab Runner on AWS EC2](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/) and also a lot of discussion on this [issue thread](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3222) with configuring it.  

## Setting up the shared runner

Read the documentation provided here: [Autoscaling Gitlab Runner on AWS EC2](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/). \
A sample [config.toml](./config.toml) file that Border uses is provided for a reference.

### Additional Notes
1. IAM user `gitlab-runner-manager` was created to manage the Gitlab runner
2. S3 Bucket `cache.borderux.com` was created as a shared cache for the runner (stores node_modules and yarn cache)
3. EC2 instance `gitlab-runner-manager` was created as the runner manager.  This is a `t2.micro` instance that is always running.  Make sure to setup [EC2 Instance Connect](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-connect-set-up.html) to make it easier to connect via the AWS console.
4. Modified the `/etc/gitlab-runner/config.toml`.  Notes on the changes
- Remove `--amazonec2-block-duration-minutes=60` because it was causing failures https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3222
- Changed `IdleTime = 600` to stop running spot instances over 10 minutes
- Disable docker cache in `[runners.docker]` using `disable_cache = true` and remove `cache =` to remove additional warning
5. Update `gitlab-ci.yml` to utilize the cache to store the `.yarn` and `node_modules` folder to speed up builds
6. Added the file `docker-machine-rm` to the `gitlab-runner-manager` server `/etc/cron.daily` task to remove any docker machines running on a nightly basis

### Add the add-project-runner script
1. Connect to your `gitlab-runner-manager` EC2 instance.  You can use EC2 connect, or SSH into the running instance
2. Download the [add-project-runner](./add-project-runner) script to your root users home directory using curl. \
`curl -L -O https://gitlab.com/borderux/gitlab-aws-runner/-/raw/main/add-project-runner`
3. Set permissions on the script so you can execute it \
`chmod u=rwx add-project-runner`
4. Modify the `add-project-runner` script and update the commands provided to the `gitlab-runner register` command.  You can find more information on options for the register command at: https://docs.gitlab.com/runner/register/
5. You will need to create a [template.toml](./template.toml) file that will be used by the `add-project-runner` script. This will be merged into the final config.toml during registration.  You can download the sample one here:
`curl -L -O https://gitlab.com/borderux/gitlab-aws-runner/-/raw/main/template.toml`

## Configuring your project to use the shared runner

![Project Runner settings](gitlab-runner-settings.png)
1. Go to `Settings > CI/CD > Runners` in your project Settings
2. Disabled the shared runners for the project by toggling the `Enable shared runners for this project`.  If you don't do this, you will continue to have problems with devs that do not have full Gitlab accounts not being able to use your runners.
3. Copy the `registration token` found under the Specific runners
4. Connect to your `gitlab-runner-manager` running instance in EC2.
5. Run the script `add-project-runner` and follow the instructions.  Make sure you updated your `template.toml` file before running this.
6. Check your Runner settings again under `Settings > CI/CD > Runners` and you should see a green status that your runner is properly attached to your project \
![Runner configured properly](gitlab-runner-status.png)
